//+build mage

package main

import (
	"strings"
	"fmt"
	"log"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

var TERRAFORM_VERSION = "0.12.19"

// Installs Terraform, update TERRAFORM_VERSION to control your version.
func Terraform () error {
	if isTerraformInstalled() {
		return nil
	}

	mg.Deps(TfEnv)

	log.Println("Installing terraform")
	var err error
	if err = sh.RunV("tfenv", "install", TERRAFORM_VERSION); err != nil {
		return err
	}

	return nil
}

func isTerraformInstalled () bool {

	if ! commandExists("terraform") {
		return false
	}

	// Check the version
	s, err := sh.Output("terraform", "-v")
	if err != nil {
		fmt.Printf("terraform -v error: %v\n", err)
		return false
	}

	return strings.HasPrefix(s, fmt.Sprintf("Terraform v%s", TERRAFORM_VERSION))

}
