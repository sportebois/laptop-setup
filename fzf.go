//+build mage

package main

import (
	"fmt"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"log"
)

// fzf, A command-line fuzzy finder.
// Read more https://github.com/junegunn/fzf
func Fzf() error {
	if isFzfInstalled() {
		return nil
	}

	mg.Deps(Brew)
	mg.Deps(SysPackages) // To make sure Zsh is already installed, so that auto completion will be correctly added

	log.Println("Installing fzf")
	var err error

	if err = sh.RunWith(homebrewEnv, "brew", "install", "fzf"); err != nil {
		err = sh.RunWith(homebrewEnv, "brew", "upgrade", "fzf")
	}

	if err != nil {
		log.Printf("error installing fzf: %s\n", err)
		return err
	}

	// To install useful key bindings and fuzzy completion:
	p, err := sh.Output("brew", "--prefix")
	if err != nil {
		log.Printf("error getting brew path for fzf: %s\n", err)
		return err
	}
	err = sh.Run(fmt.Sprintf("%s/opt/fzf/install", p))

	log.Printf("Install fzf, status: %v\n", err)

	if err != nil {
		log.Println("To install keyboard shortcuts, run /home/linuxbrew/.linuxbrew/opt/fzf/install")
	}

	return err
}

// removeFzf removes fzf, if installed with brew
func RemoveFzf() error {
	if !isFzfInstalled() {
		return nil
	}
	if !isBrewInstalled() {
		msg := "We only support fzf install/remove via brew. fzf found, but not brew. Please remove it manually."
		log.Println(msg)
		return fmt.Errorf(msg)
	}

	return sh.RunWith(homebrewEnv, "brew", "uninstall", "fzf")
}

func isFzfInstalled() bool {
	return commandExists("fzf")
}
