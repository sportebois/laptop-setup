//+build mage

package main

import (
	"fmt"
	"log"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// fx is a command-line tool and terminal JSON viewer
// Read more https://github.com/antonmedv/fx
func Fx () error {
	if isFxInstalled() {
		return nil
	}

	mg.Deps(Brew)

	log.Println("Installing fx")
	var err error

	if err = sh.RunWith(homebrewEnv, "brew", "install", "fx"); err != nil {
		err = sh.RunWith(homebrewEnv, "brew", "upgrade", "fx")
	}
	log.Printf("Install fx, status: %v\n", err)

	return err
}

// RemoveFx removes fx, if installed with brew
func RemoveFx () error {
	if !isFxInstalled() {
		return nil
	}
	if !isBrewInstalled() {
		msg :="We only support fx install/remove via brew. fx found, but not brew. Please remove it manually."
		log.Println(msg)
		return fmt.Errorf(msg)
	}

	return sh.RunWith(homebrewEnv, "brew", "uninstall", "fx")
}

func isFxInstalled () bool {
	return commandExists("fx")
}
