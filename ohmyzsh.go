//+build mage

package main

import (

	"regexp"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	getter "github.com/hashicorp/go-getter"
	homedir "github.com/mitchellh/go-homedir"
)

// oh-my-zsh is the must-have framework to extend your zsh
// oh-my-zsh plugins will be activated after the install
// read more: https://github.com/robbyrussell/oh-my-zsh
func OhMyZsh() error {

	if isOhMyZshInstalled() {
		return nil
	}

	mg.Deps(SysPackages)

	var err error
	file, err := ioutil.TempFile("/tmp", "install-oh-my-zsh.*.sh")
	if err != nil {
		log.Printf("error creating temporary file for oh-my-zsh install script: %s\n", err)
		return err
	}
	defer os.Remove(file.Name())
	err = getter.GetFile(file.Name(), "https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh")
	if err != nil {
		log.Printf("error downloading oh-my-zsh install script: %s\n", err)
		return err
	}
	err = sh.Run("sh", file.Name())
	if err != nil {
		log.Printf("error running oh-my-zsh install script: %s\n", err)
		return err
	}

	return OhMyZshPlugins()
}

var ohMyZshPlugins = "git aws docker"

// OhMyZshPlugins sets your oh-my-zsh plugin in your ~/.zshrc
// It does not (yet) download them!
// Edit ohMyZshPlugins to add/remove your plugins
// Plugins are not part of the default run, and are either run:
//   - automatically, when you install oh-my-zsh (part of the default)
//   - manually if you run `mage ohmyzshplugins`
func OhMyZshPlugins () error {

	mg.Deps(OhMyZsh)

	p, err := homedir.Expand("~/.zshrc")
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return err
	}
	// Get sure we’re the only one manipulating this file right now!
	defer GetSafeFile(p)()


	zshrc, err := ioutil.ReadFile(p)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return err
	}

	// Search for ^plugins=(..)
	var re = regexp.MustCompile(`(?m)^plugins=\(([\w\s]*)\)`)

	s := re.ReplaceAllString(string(zshrc), fmt.Sprintf("plugins=(%s)\n", ohMyZshPlugins))

	// Write it back to file
	err = ioutil.WriteFile(p, []byte(s), 0644)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return err
	}
	log.Printf("oh-my-zsh plugins set to %s\n", ohMyZshPlugins)
	return nil
}

func isOhMyZshInstalled() bool {
	p, err := homedir.Expand("~/.oh-my-zsh/oh-my-zsh.sh")
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return false
	}
	fi, err := os.Stat(p)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return false
	}
	return fi.Size() > 0
}
