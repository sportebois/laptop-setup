//+build mage

package main

import (
	"log"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)


// Installs Landscape, a utility which improves Terraform's plan output to be easier to read and understand.
// https://github.com/coinbase/terraform-landscape
func Landscape () error {
	if isLandscapeInstalled() {
		return nil
	}

	mg.Deps(Ruby)

	log.Println("Installing landscape")
	var err error
	if err = sh.RunV("gem", "install", "terraform_landscape"); err != nil {
		return err
	}

	return nil
}

func isLandscapeInstalled () bool {
	return commandExists("landscape")
}
