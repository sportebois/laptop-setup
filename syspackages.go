//+build mage

package main

import (
	"errors"
	"log"

	"github.com/magefile/mage/sh"
)

// SysPackages install system packages using either apt, dnf or yum
func SysPackages() error {
	if usesApt() {
		return aptGetInstall()
	}
	if usesDnf() {
		return dnfInstall()
	}
	if usesYum() {
		return yumInstall()
	}
	return errors.New("no supported package installer found")
}

func dnfInstall() error {

	log.Println("Installing dnf packages")
	var err error
	var packages, cmdArgs []string

	// Step 1, the "foundation"
	packages = []string{
		"@development-tools", // "build-essential" equivalent
		"libatomic",
		"curl", "wget", "file", "git", "vim",
		"xclip",
		"bat",
		"console-setup",
		"pandoc",
		"pulseaudio",
		"pulseaudio-module-bluetooth",
		"pavucontrol",
		"blueman",
		"xrdp",            // you’ll then need to `systemctl start xrdp` and `systemctl start xrdp`
		"util-linux-user", // useful for chsh (to change shell with: `chsh -s $(which zsh)`)
		"zsh",
		// "console-data",
		// "keyboard-configuration",
		// "bluez-firmware",
	}
	/*
				RDP setup, you then need to (not yet automated)
				sudo systemctl enable xrdp
				sudo systemctl start xrdp
				sudofirewall-cmd --add-port=3389/tcp --permanent
				sudo firewall-cmd --reload

		gsettings set org.gnome.Vino require-encryption false
		cat "allowed_users = anybody" >> /etc/X11/Xwrapper.config
		# then restart xrdp
	*/

	cmdArgs = append([]string{"dnf", "install", "-y"}, packages...)
	if err = sh.Run("sudo", cmdArgs...); err != nil {
		return err
	}

	// Step 2, the utils (some might rely on the foundations)
	packages = []string{
		"fd-find",
		"flameshot",
		"flatpack",
		"graphviz",
		"htop",
		"jq",
		"meld",
		"ripgrep",
		// "shellcheck",
		"solaar", // For Logitech Unifying devices
		"terminator",
	}

	cmdArgs = append([]string{"dnf", "install", "-y"}, packages...)
	if err = sh.Run("sudo", cmdArgs...); err != nil {
		return err
	}

	return nil
}

func yumInstall() error {
	log.Println("Installing yum packages")
	var err error
	var packages, cmdArgs []string

	// Step 1, the "foundation"
	packages = []string{
		"curl", "wget", "file", "git", "vim",
		"xclip",
		"console-setup",
		"pulseaudio",
		"pulseaudio-module-bluetooth",
		"pavucontrol",
		"blueman",
		"util-linux-user", // useful for chsh (to change shell with: `chsh -s $(which zsh)`)
		"zsh",
		// "console-data",
		// "keyboard-configuration",
		// "bluez-firmware",
	}

	cmdArgs = append([]string{"yum", "install", "-y"}, packages...)
	if err = sh.Run("sudo", cmdArgs...); err != nil {
		return err
	}

	// Step 2, the utils (some might rely on the foundations)
	packages = []string{
		"fd-find",
		"graphviz",
		"htop",
		"jq",
		"meld",
		"ripgrep",
		// "shellcheck",
		"solaar", // For Logitech Unifying devices
		"terminator",
	}

	cmdArgs = append([]string{"yum", "install", "-y"}, packages...)
	if err = sh.Run("sudo", cmdArgs...); err != nil {
		return err
	}

	return nil
}

// aptGetInstall installs all the regular apt-get install whatever dependencies
func aptGetInstall() error {

	log.Println("Installing apt-get packages")
	var err error
	var packages, cmdArgs []string

	// Step 1, the "foundation"
	packages = []string{
		"build-essential", "apt-transport-https",
		"curl", "wget", "file", "git", "vim",
		"console-data", "console-setup", "keyboard-configuration",
		"pulseaudio", "pulseaudio-module-bluetooth", "pavucontrol", "bluez-firmware", "blueman",
		"zsh",
	}

	cmdArgs = append([]string{"apt-get", "install"}, packages...)
	if err = sh.Run("sudo", cmdArgs...); err != nil {
		return err
	}

	// Step 2, the utils (some might rely on the foundations)
	packages = []string{
		"fd",
		"graphviz",
		"htop",
		"jq",
		"meld",
		"ripgrep",
		"shellcheck",
		"solaar", // For Logitech Unifying devices
		"terminator",
	}

	cmdArgs = append([]string{"apt-get", "install"}, packages...)
	if err = sh.Run("sudo", cmdArgs...); err != nil {
		return err
	}

	return nil
}

func usesApt() bool {
	return commandExists("apt")
}

func usesDnf() bool {
	return commandExists("dnf")
}

func usesYum() bool {
	return commandExists("yum")
}
