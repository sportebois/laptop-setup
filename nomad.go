//+build mage

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	getter "github.com/hashicorp/go-getter"
	"github.com/magefile/mage/sh"
)

var NOMAD_VERSION = "0.10.1"

// var NOMAD_VERSION = "0.8.6"

// Nomad is a flexible workload orchestrator that enables an organization to
// easily deploy and manage any containerized or legacy application using
// a single, unified workflow.
// Nomad can run a diverse workload of Docker, non-containerized,
// microservice, and batch applications.
// read more: https://www.nomadproject.io/
func Nomad() error {

	if isNomadInstalled() {
		return nil
	}

	url := fmt.Sprintf("https://releases.hashicorp.com/nomad/%s/nomad_%s_linux_amd64.zip", NOMAD_VERSION, NOMAD_VERSION)

	var err error
	dir, err := ioutil.TempDir("/tmp", "nomad")
	if err != nil {
		log.Printf("error creating temporary folder for Nomad: %s\n", err)
		return err
	}
	defer os.RemoveAll(dir)

	log.Printf("Downloading Nomad from %s to %v \n", url, dir)
	err = getter.GetAny(dir, url)
	if err != nil {
		log.Printf("error downloading Nomad install script: %s\n", err)
		return err
	}

	err = os.Rename(dir+"/nomad", "/usr/local/bin/nomad")
	if err != nil {
		log.Printf("error moving Nomad to /usr/local/bin/: %s\n", err)
		return err
	}

	return nil
}

func isNomadInstalled() bool {
	if !commandExists("nomad") {
		return false
	}

	// Check the version
	s, err := sh.Output("nomad", "-v")
	if err != nil {
		fmt.Printf("nomad -v error: %v\n", err)
		return false
	}

	return strings.HasPrefix(s, fmt.Sprintf("Nomad v%s", NOMAD_VERSION))
}
