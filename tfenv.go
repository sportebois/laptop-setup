//+build mage

package main

import (
	"fmt"
	"log"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// tfenv lets you manage Terraform versions
func TfEnv () error {
	if isTfenvInstalled() {
		return nil
	}

	mg.Deps(Brew)

	log.Println("Installing tfenv")
	var err error

	if err = sh.RunWith(homebrewEnv, "brew", "install", "tfenv"); err != nil {
		err = sh.RunWith(homebrewEnv, "brew", "upgrade", "tfenv")
	}
	log.Printf("Install tfenv, status: %v\n", err)

	return err
}

// removeTfenv removes tfenv, if installed with brew
func RemoveTfEnv () error {
	if !isTfenvInstalled() {
		return nil
	}
	if !isBrewInstalled() {
		msg :="We only support tfenv install/remove via brew. tfenv found, but not brew. Please remove it manually."
		log.Println(msg)
		return fmt.Errorf(msg)
	}
	
	return sh.RunWith(homebrewEnv, "brew", "uninstall", "tfenv")
}

func isTfenvInstalled () bool {
	return commandExists("tfenv")
}
