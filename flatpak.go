//+build mage

package main

import (
	"errors"
	"log"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)


func Flatpak() error {
	var err error
	if !usesDnf() {
		return errors.New("flatpak installation is only configured for dnf")
	}

	mg.Deps(dnfInstall)

	if !isFlatpakInstalled() {
		return errors.New("flatpak not installed after the dnf dependencies run. Aborting.")
	}

	var flatpak = sh.RunCmd("flatpak")
	// Register flathub to find things
	if err = flatpak("remote-add", "flathub", "https://flathub.org/repo/flathub.flatpakrepo"); err != nil && sh.ExitStatus(err) != 1 {
		// error status 1 means that it’s already installed, not really an error
		log.Println(err)
		return err
	}

	// Install Slack
	if err = flatpak("install", "-y", "flathub", "com.slack.Slack"); err != nil {
		return err
	}

	// Install Spotify
	if err = flatpak("install", "-y", "flathub", "com.spotify.Client"); err != nil {
		return err
	}

	return nil
}


func isFlatpakInstalled() bool {
	return commandExists("flatpak")
}
