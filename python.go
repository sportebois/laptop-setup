//+build mage

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/magefile/mage/sh"
	getter "github.com/hashicorp/go-getter"
	homedir "github.com/mitchellh/go-homedir"
)

/*


    PYTH_V=3.7.0
    cd ~/Downloads
    wget wget https://www.python.org/ftp/python/${PYTH_V}/Python-${PYTH_V}.tgz
    tar xvf Python-${PYTH_V}.tgz
    cd Python-${PYTH_V}
    ./configure --enable-optimizations --with-ensurepip=install
    make -j8
    sudo make install
    sudo update-alternatives --install /usr/bin/python python /usr/local/bin/python3.7 137

    # Later for reminders:
    update-alternatives --config python

*/

var PYTHON_VERSION = "3.7.0"

// Python
// To check your installed version: sudo update-alternatives --config python
// After installing Python 3.7 and making it the default with update-alternatives,
// I sometimes had to run the following to fix older stuff installed:
//   sudo apt-get clean
//   sudo apt-get update
//   sudo apt-get install --reinstall python-minimal python-lockfile
func Python() error {

	if isPythonInstalled() {
		return nil
	}

	url := fmt.Sprintf("https://www.python.org/ftp/python/%s/Python-%s.tgz", PYTHON_VERSION, PYTHON_VERSION)

	var err error
	var cmdArgs []string
	dir, err := ioutil.TempDir("/tmp", "python")
	if err != nil {
		log.Printf("error creating temporary folder for Python: %s\n", err)
		return err
	}
	defer os.RemoveAll(dir)

	log.Printf("Downloading Python from %s to %v \n", url, dir)
	err = getter.GetAny(dir, url)
	if err != nil {
		log.Printf("error downloading Python tarball: %s\n", err)
		return err
	}

	p, err := homedir.Expand(dir + "/Python-" + PYTHON_VERSION)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return err
	}

	os.Chdir(p)

	if err = sh.Run("./configure", "--enable-optimizations", "--with-ensurepip=install"); err != nil {
		log.Printf("error running configure: %s\n", err)
		return err
	}

	if err = sh.Run("make", "-j8"); err != nil {
		log.Printf("error running make: %s\n", err)
		return err
	}

	cmdArgs = []string{"make", "install"}
	if err = sh.Run("sudo", cmdArgs...); err != nil {
		log.Printf("error running make install: %s\n", err)
		return err
	}

	cmdArgs = []string{"update-alternatives", "--install", "/usr/bin/python", "python", "/usr/local/bin/python3.7", "137"}
	if err = sh.Run("sudo", cmdArgs...); err != nil {
		log.Printf("error installing update-alternatives for Python: %s\n", err)
		return err
	}

	return nil
}


func isPythonInstalled() bool {
	if !commandExists("python") {
		return false
	}

	// Check the version
	s, err := sh.Output("python", "--version")
	if err != nil {
		fmt.Printf("python --version error: %v\n", err)
		return false
	}

	return strings.HasPrefix(s, fmt.Sprintf("Python %s", PYTHON_VERSION))
}
