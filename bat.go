//+build mage

package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/magefile/mage/sh"
)

var BAT_VERSION = "0.11.0"

// bat, a cat clone with wings.
// Change BAT_VERSION to select which version to install
// Read more https://github.com/sharkdp/bat
func Bat() error {
	if isBatInstalled() {
		return nil
	}

	err := installDebPkg("bat", "https://github.com/sharkdp/bat/releases/download/v"+BAT_VERSION+"/bat_"+BAT_VERSION+"_amd64.deb")

	if err == nil {
		log.Println("Installed bat")
	}

	return nil
}

// removeBat removes bat
func RemoveBat() error {
	if !isBatInstalled() {
		return nil
	}
	log.Println("Removing bat")
	err := sh.Run("sudo", "dpkg", "--remove", "bat")
	if err != nil {
		log.Printf("error removing bat package: %s\n", err)
		return err
	}
	log.Println("Removed bat")

	return nil
}

func isBatInstalled() bool {
	if !commandExists("bat") {
		return false
	}

	// Check the version
	s, err := sh.Output("bat", "--version")
	if err != nil {
		fmt.Printf("bat --version error: %v\n", err)
		return false
	}

	return strings.HasPrefix(s, fmt.Sprintf("bat %s", BAT_VERSION))
}
