//+build mage

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	getter "github.com/hashicorp/go-getter"
	"github.com/magefile/mage/sh"
	homedir "github.com/mitchellh/go-homedir"
)

var VAULT_VERSION = "1.3.0"

// Vault is a tool for secrets management, encryption as a service, and privileged access management.
// read more: https://www.vaultproject.io/
func Vault() error {

	if isVaultInstalled() {
		return nil
	}

	url := fmt.Sprintf("https://releases.hashicorp.com/vault/%s/vault_%s_linux_amd64.zip", VAULT_VERSION, VAULT_VERSION)

	var err error
	dir, err := ioutil.TempDir("/tmp", "vault")
	if err != nil {
		log.Printf("error creating temporary folder for Vault: %s\n", err)
		return err
	}
	defer os.RemoveAll(dir)

	log.Printf("Downloading Vault from %s to %v \n", url, dir)
	err = getter.GetAny(dir, url)
	if err != nil {
		log.Printf("error downloading Vault install script: %w\n", err)
		return err
	}

	// Open the Vault binary to copy it
	srcPath := dir + "/vault"
	destFolder := "~/.local/bin/"
	destFolder, err = homedir.Expand(destFolder)
	if err != nil {
		fmt.Printf("error expanding path %s: %v\n", destFolder, err)
		return err
	}
	if err = os.MkdirAll(destFolder, 0755); err != nil {
		log.Printf("error creating %s directory: %s\n", destFolder, err)
		return err
	}
	destPath := destFolder + "/vault"

	err = sh.Copy(destPath, srcPath)
	if err != nil {
		log.Printf("error writing from %s to %s: %w\n", srcPath, destPath, err)
		return err
	}

	err = sh.Rm(dir)
	if err != nil {
		log.Printf("error removing temporary download: %s\n", err)
		return err
	}

	return nil
}

func isVaultInstalled() bool {
	if !commandExists("vault") {
		return false
	}

	// Check the version
	s, err := sh.Output("vault", "-v")
	if err != nil {
		fmt.Printf("vault -v error: %v\n", err)
		return false
	}

	return strings.HasPrefix(s, fmt.Sprintf("Vault v%s", VAULT_VERSION))
}
