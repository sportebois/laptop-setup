//+build mage

package main

import (
	"fmt"
	"log"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// fx is a command-line tool and terminal JSON viewer
// Read more https://github.com/antonmedv/fx
func GraphViz() error {
	if isFxInstalled() {
		return nil
	}

	mg.Deps(Brew)

	log.Println("Installing fx")
	var err error

	if err = sh.RunWith(homebrewEnv, "brew", "install", "graphviz"); err != nil {
		err = sh.RunWith(homebrewEnv, "brew", "upgrade", "graphviz")
	}
	log.Printf("Install graphviz, status: %v\n", err)

	return err
}

// RemoveFx removes fx, if installed with brew
func RemoveGraphViz() error {
	if !isGraphVizInstalled() {
		return nil
	}
	if !isBrewInstalled() {
		msg := "We only support GraphViz install/remove via brew. fx found, but not brew. Please remove it manually."
		log.Println(msg)
		return fmt.Errorf(msg)
	}

	return sh.RunWith(homebrewEnv, "brew", "uninstall", "graphviz")
}

func isGraphVizInstalled() bool {
	return commandExists("fx")
}
