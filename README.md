# laptop-setup

Automatic laptop configuration and tool installation with little dependencies

## Installation & pre-requisites

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Go](https://golang.org/doc/install)
- [mage](https://magefile.org/) (requires Go)

Clone the repo, e.g. `git clone git@gitlab.com:s.portebois/laptop-setup.git ~/.laptopsetup`

CD into that directory, and run `mage`

## How to run it

To run the default bundles: `mage`

To run a specific tasks (and all its pre-requisites): `mage taskname`

To list the tasks available: `mage -l`

## Important files

- `setup.go` list the tasks which will be run by default.
