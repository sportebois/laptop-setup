//+build mage,linux

package main

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/codeclysm/extract"
	"github.com/magefile/mage/sh"

	getter "github.com/hashicorp/go-getter"
	homedir "github.com/mitchellh/go-homedir"
)

var zoteroVersion = "5.0.80"

// Zotero is a free, easy-to-use win/mac/linux tool to help you collect, organize, cite, and share research
func Zotero() error {
	if isZoteroInstalled() {
		return nil
	}

	url := "https://www.zotero.org/download/client/dl?channel=release&platform=linux-x86_64&version=" + zoteroVersion

	var err error
	file, err := ioutil.TempFile("/tmp", "zotero.*.tar.bz2")
	if err != nil {
		log.Printf("error creating temporary file for zotero install script: %s\n", err)
		return err
	}
	defer os.Remove(file.Name())
	err = getter.GetFile(file.Name(), url)
	if err != nil {
		log.Printf("error downloading zotero install tarball: %s\n", err)
		return err
	}


	localBin := "~/.local/bin"
	localBin, err = homedir.Expand(localBin)
	if err != nil {
		fmt.Printf("error expanding path %s: %v\n", localBin, err)
		return err
	}
	destFolder := localBin + "/zotero_linux"

	archive, _ := ioutil.ReadFile(file.Name())
	buffer := bytes.NewBuffer(archive)
	ctx := context.Background()
	extract.Archive(ctx, buffer, destFolder, nil)


	iconScript := destFolder + "/Zotero_linux-x86_64/set_launcher_icon"
	if err = sh.RunV(iconScript); err != nil {
		fmt.Printf("error setting icon for zotero desktop: %v\n", err)
		return err
	}

	desktp := destFolder + "/Zotero_linux-x86_64/zotero.desktop"
	p, err := homedir.Expand("~/.local/share/applications/zotero.desktop")
	if err != nil {
		fmt.Printf("error expanding zotero desktop path: %v\n", err)
		return err
	}
	if err = sh.RunV("ln", "-fs", desktp, p); err != nil {
		fmt.Printf("error creating launcher symlink for zotero desktop: %v\n", err)
		return err
	}

	bin := destFolder + "/Zotero_linux-x86_64/zotero"
	if err = sh.RunV("ln", "-fs", bin, localBin + "/zotero"); err != nil {
		fmt.Printf("err creating zotero symlink: %v\n", err)
		return err
	}

	return nil
}

func isZoteroInstalled() bool {
	return commandExists("zotero")
}

func RemoveZotero () error {
	var err error
	if err = sh.RunV("rm", "/usr/local/bin/Zotero_linux/", "-rf"); err != nil {
		fmt.Printf("err removing Zotero contents: %v\n", err)
		return err
	}
	if err = sh.RunV("rm", "/usr/local/bin/zotero", "-f"); err != nil {
		fmt.Printf("err removing Zotero symlink: %v\n", err)
		return err
	}
	p, _ := homedir.Expand("~/.local/share/applications/zotero.desktop")
	if err = sh.RunV("rm", p, "-f"); err != nil {
		fmt.Printf("err removing Zotero launcher link: %v\n", err)
		return err
	}
	return nil
}
