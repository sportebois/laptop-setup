//+build mage
// More info about Homebrew on Linux: https://docs.brew.sh/Homebrew-on-Linux

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	getter "github.com/hashicorp/go-getter"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

var homebrewEnv = map[string]string{"HOMEBREW_NO_ENV_FILTERING": "1"}

// brew is the Linux port of Homebrew (formerly Linuxbrew), a package manager
// Read more at https://docs.brew.sh/Homebrew-on-Linux
func Brew() error {
	if isBrewInstalled() {
		return nil
	}

	mg.Deps(SysPackages) // We want bash or zsh to be installe for the completion

	/*
	   // Installation
	   sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
	    "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile

	*/
	var name = "brew"
	var url = "https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh"
	var err error
	file, err := ioutil.TempFile("/tmp", "install_"+name+".*.sh")
	fileName := file.Name()
	if err != nil {
		log.Printf("error creating temporary file for %s shell script: %s\n", name, err)
		return err
	}
	defer os.Remove(fileName)
	err = getter.GetFile(fileName, url)
	if err != nil {
		log.Printf("error downloading %s shell script: %s\n", name, err)
		return err
	}

	err = os.Chmod(fileName, 0744)
	if err != nil {
		fmt.Println(err)
	}

	err = sh.Run(fileName)
	if err != nil {
		log.Printf("error running %s shell script: %s\n", name, err)
		return err
	}

	// add shell completion

	if exists, brewp := fileExists("~/.linuxbrew"); exists {
		installBrewEnv(brewp, "~/.bash_profile")
		installBrewEnv(brewp, "~/.zshrc")
		installBrewEnv(brewp, "~/.profile")
	}
	if exists, brewp := fileExists("/home/linuxbrew/.linuxbrew"); exists {
		installBrewEnv(brewp, "~/.bash_profile")
		installBrewEnv(brewp, "~/.zshrc")
		installBrewEnv(brewp, "~/.profile")
	}

	/*
	   // Shell installation
	   test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
	   test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
	   test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
	   echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile
	*/
	return nil
}

func installBrewEnv(brewPath, destPath string) {
	log.Println("installBrewEnv", brewPath, destPath)
	destExists, destPath := fileExists(destPath)
	if !destExists {
		return
	}

	brewenv, err := sh.Output(brewPath+"/bin/brew", "shellenv")
	if err != nil {
		log.Printf("error getting brew path for fzf: %s\n", err)
		return
	}
	// brew shell env stupidly overwrite the PATH, with something like
	// export PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:"
	// let’s concatenate it with our current path instead
	brewenv = strings.Replace(brewenv, `export PATH="`, `export PATH="$PATH:`, -1)
	brewenv = brewenv + "\nexport HOMEBREW_NO_ENV_FILTERING=1\n"

	err = installConfInFile("homebrew", destPath, brewenv)
	if err != nil {
		log.Println("installBrewEnv", destPath, destPath, "ERROR", err)
	}
	log.Println("installBrewEnv", destPath, destPath, "success")

}

func isBrewInstalled() bool {
	return commandExists("brew")
}
