//+build mage

package main

import (
	"github.com/magefile/mage/mg"
)

var Default = All

func All() {
	mg.Deps(SysPackages)
	mg.Deps(Brew)
	// mg.Deps(TfEnv)
	// mg.Deps(Ruby)

	mg.Deps(OhMyZsh)
	mg.Deps(Flatpak)

	mg.Deps(Terraform)
	// mg.Deps(Landscape)

	mg.Deps(Bat)
	mg.Deps(Fx)
	mg.Deps(Fzf)
	// mg.Deps(Nomad)
	mg.Deps(Vault)
	mg.Deps(Zotero)
}
