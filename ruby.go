//+build mage

package main

import (
	"strings"
	"fmt"
	"log"
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

var RUBY_VERSION = "2.6.3"

// Installs Ruby, update RUBY_VERSION to control your version.
func Ruby () error {
	if isRubyInstalled() {
		return nil
	}

	mg.Deps(Brew)

	log.Println("Installing ruby")
	var err error

	if err = sh.RunV("brew", "install", RUBY_VERSION); err != nil {
		return err
	}
	log.Printf("Install ruby, status: %v\n", err)


	return nil
}

func isRubyInstalled () bool {

	if ! commandExists("ruby") {
		return false
	}

	// Check the version
	s, err := sh.Output("ruby", "-v")
	if err != nil {
		fmt.Printf("ruby -v error: %v\n", err)
		return false
	}

	return strings.HasPrefix(s, fmt.Sprintf("ruby %s", RUBY_VERSION))

}
