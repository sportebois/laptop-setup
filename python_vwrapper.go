//+build mage

package main

import (
	"fmt"
	"log"
	"os"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	homedir "github.com/mitchellh/go-homedir"
)


// VirtualEnvWrapper installs Python’s virtualenv and virtualenvwrapper
// Then all you need to do is `mkvirtualenv --python=/usr/local/bin/python3.7 yourVenvName`
// and later `workon yourVenvName` to load it
func VirtualEnvWrapper() error {
	mg.Deps(Python)

	if isVirtualEnvWrapperInstalled() {
		return nil
	}

	var err error

	// Install virtualenv and virtualenvwrapper
	if err = sh.Run("sudo", "pip", "install", "virtualenv", "virtualenvwrapper"); err != nil {
		log.Printf("error running pip install virtualenv: %s\n", err)
		return err
	}

	// Create a dir to store the virtualenvs
	p, err := homedir.Expand("~/.venv")
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return err
	}
	if err = os.MkdirAll(p, 0755); err != nil {
		log.Printf("error creating ~/.venv directory: %s\n", err)
		return err
	}

	venvwrapperCfg := "\nexport WORKON_HOME=~/.venv \n. /usr/local/bin/virtualenvwrapper.sh\n"
	cfgPath := "~/.zshrc"
	err = installConfInFile("virtualenvwrapper", cfgPath, venvwrapperCfg)
	if err != nil {
		log.Printf("error adding virtualenvwrapper.sh to %s: %s\n", cfgPath, err)
		return err
	}

	return nil
}


func isVirtualEnvWrapperInstalled() bool {
	return commandExists("workon") // FIXME check it exist (but it’s not a path to an exec!)
}
