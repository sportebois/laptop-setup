//+build mage

package main

import (
	"io/ioutil"
	"log"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"sync"

	getter "github.com/hashicorp/go-getter"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/magefile/mage/sh"
)

func commandExists(cmd string) bool {
	_, err := exec.LookPath(cmd)

	return err == nil
}

func installDebPkg(name, url string) error {

	log.Println("Installing " + name + " from " + url)

	var err error
	file, err := ioutil.TempFile("/tmp", name+"_amd64.*.deb")
	if err != nil {
		log.Printf("error creating temporary file for %s package: %s\n", name, err)
		return err
	}
	defer os.Remove(file.Name())
	err = getter.GetFile(file.Name(), url)
	if err != nil {
		log.Printf("error downloading %s package: %s\n", name, err)
		return err
	}

	err = sh.Run("sudo", "dpkg", "-i", file.Name())
	if err != nil {
		log.Printf("error running %s package: %s\n", name, err)
		return err
	}
	return nil
}

var safeFiles map[string]sync.Mutex

// GetSafeFile is a wrapper to lock/unlock access to a file.
// It’s useful the coordinate processes who want to get write access to a file
// Simplest way to use it:  defer GetSafeFile("/path/to/your/file")()
func GetSafeFile(path string) func() {
	l := safeFiles[path]
	l.Lock()
	return func() { l.Unlock() }
}


// installConfInFile add lines into path files, after locking it from other calls,
// and wraps it with `//` comments identified with anchor to be able to update it in place later.
func installConfInFile(anchor, path, lines string) error {
	anchorStart := "\n# <" + anchor + ">\n"
	anchorEnd := "\n# </" + anchor + ">\n"

	var err error
	path, err = homedir.Expand(path)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return err
	}

	// Get sure we’re the only one manipulating this file right now!
	defer GetSafeFile(path)()

	b, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	s := string(b)
	// check whether s contains substring text
	if strings.Contains(s, anchorStart) {
		// return nil // No edit to do
		log.Println("brew config set, overriding it")

		re := regexp.MustCompile(`(?sm)\n^# <` + anchor + `>.*^# </` + anchor + `>\n`)
		s = re.ReplaceAllLiteralString(string(b), anchorStart+lines+anchorEnd)
	} else {
		s = s + "\n"+ anchorStart+lines+anchorEnd + "\n"
	}

	// Write it back to file
	err = ioutil.WriteFile(path, []byte(s), 0644)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return err
	}
	return nil
}

// fileExists accepts ~ for home and will automatically resolve it on the local platform
func fileExists(path string) (bool, string) {
	p, err := homedir.Expand(path)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		return false, ""
	}
	if _, err := os.Stat(p); err != nil {
		return false, ""
	}
	return true, p
}
